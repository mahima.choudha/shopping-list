import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RecipeService} from '../recipes/recipe.service';
import {Recipe} from '../recipes/recipe.model';
import {map, tap} from 'rxjs/operators';

@Injectable({providedIn: 'root'}) // alternative for providing it in app.module.ts
export class DataStorageService {
  constructor(private http: HttpClient,
              private  recipesService: RecipeService) {}

  storeRecipe(): any {
    const recipes = this.recipesService.getRecipes();
    // put is used cos we want to save or update all recipes
    this.http.put('https://shoppint-list.firebaseio.com/recipes.json', recipes).subscribe(
      (response) => {
        console.log(response);
      });
    // As there are no html operation needed we can subscribe here
    // but if we have to manipulate output in the template then we can return the observable from service
    // and implement it in the component
  }

  fetchRecipes(): any {
    // combining two observables
    // take only allows to get one value from observable and then it automatically unsubscribe
    // exhaustMap op waits for first observable(user) to complete then it gives us that object(user)
    // after first observable is done it will be replaced with inner observable(http) in observable chain
    // For firebase, token is added in the query parameter
    // return this.authService.user.pipe(take(1), exhaustMap(user => {
    return this.http.get<Recipe[]>(
      'https://shoppint-list.firebaseio.com/recipes.json')
      .pipe(
        map(recipes => { // here, map is rxjs function
          return recipes.map(recipe => { // here, map is JS function
            return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
          });
        }), tap(recipes => { // tap allows to execute code without altering the data
          this.recipesService.setRecipes(recipes);
        }));
    // to add ingredients we can use pipe. If we don't add ingredients then we might get error while accessing them directly
  }
}

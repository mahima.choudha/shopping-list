import {Ingredient} from '../shared/ingredient.model';
import {Subject} from 'rxjs';


export class ShoppingListService {
  ingredientsChanged = new Subject<Ingredient[]>();
  startEditing = new Subject<number>();
  private ingredients: Ingredient[] = [
    new Ingredient('cheese', 5),
    new Ingredient('bread', 2)
  ];
  getIngredients(): any {
    // slice() returns copy of array which isn't editable
    // to add new ingredients we can simply return this.ingredients
    return this.ingredients.slice();
  }
  addIngredient(ingredient: Ingredient): any {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  getIngredient(index: number): any {
    return this.ingredients[index];
  }

  addIngredients(ingredients: Ingredient[]): any {
    // this takes a lot of assigning
    // for (const ingredient of ingredients){
    //   this.addIngredient(ingredient);
    // }
    // using spread operator ... to convert array to list
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  updateIngredient(index: number, newIngredient: Ingredient): any {
    this.ingredients[index] = newIngredient;
    this.ingredientsChanged.next(this.ingredients.slice());
  }

  deleteIngredient(index: number): any {
    this.ingredients.splice(index, 1);
    this.ingredientsChanged.next(this.ingredients.slice());
  }
}

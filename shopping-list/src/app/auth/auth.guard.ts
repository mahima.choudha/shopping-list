import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {map, take, tap} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthGuard implements  CanActivate{
  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree>{
    // using map to transform the user into boolean
    // take op takes the latest user value then unsubscribe the observable.
    // without take op user will keep emitting values which can lead to strange error
    return this.authService.user.pipe(
      take(1),
      map((user) => {
      // new approach to redirects user when they try to visit this URL that is blocked
      const isAuth = !!user; // !! converts an object which in null or have some value to false and then change it back to true
      if (isAuth) {
        return true;
      }
      return this.router.createUrlTree(['/auth']);
    }));
    // old approach to redirects user when they try to visit this URL that is blocked
    // this could result in race condition with too many redirection
    // which may interfere with each other
    //   , tap(isAuth => {
    //     if (!isAuth) {
    //       this.router.navigate(['/auth']);
    //     }
    // }));



  }
}

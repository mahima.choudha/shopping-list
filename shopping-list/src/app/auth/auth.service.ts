import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {BehaviorSubject, throwError} from 'rxjs';
import {User} from './user.model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

// This interface is optional, it is a good practice to define the response
// You can find the response body details in firebase documentation
export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean; // only used in signin so it is optional for signup
}

@Injectable({providedIn: 'root'})
export class AuthService {
  user = new BehaviorSubject<User>(null); // It also gives access to previously emitted values
  tokenExiprationTimer: any;
  constructor(private http: HttpClient,
              private router: Router) {
  }

  signup(email: string, password: string): any {
    return this.http.post<AuthResponseData>(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + environment.firebaseAPIKey,
      {
        email,
        password,
        returnSecureToken: true
      }).pipe(catchError(this.handleError), tap((resData: AuthResponseData) => {
        this.handleAuthentication(resData.email, resData.localId, resData.idToken, +resData.expiresIn);
    }));
  }

  login(email: string, password: string): any {
    return this.http.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/' +
      'accounts:signInWithPassword?key=' + environment.firebaseAPIKey,
      {
        email,
        password,
        returnSecureToken: true
      }).pipe(catchError(this.handleError), tap((resData: AuthResponseData) => {
      this.handleAuthentication(resData.email, resData.localId, resData.idToken, +resData.expiresIn);
    }));
  }

  logout(): any {
    this.user.next(null);
    this.router.navigate(['/auth']);
    // clear local storage so that user cannot directly login after logging out
    localStorage.removeItem('userData');
    if (this.tokenExiprationTimer) {
      clearTimeout(this.tokenExiprationTimer);
    }
    this.tokenExiprationTimer = null;
  }

  autoLogout(expirationDuration: number): any {
    this.tokenExiprationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  private handleAuthentication(email: string, userId: string, token: string, expiresIn: number): any {
    const expirationDate = new Date(
      new Date().getTime() + expiresIn * 1000);
    const user = new User(
      email,
      userId,
      token,
      expirationDate
    );
    this.user.next(user);
    // the timer for logout should start right after we emit user
    this.autoLogout(expiresIn * 1000);
    // stores user object in cookies to persist authentication even
    // after reloading the page
    localStorage.setItem('userData', JSON.stringify(user));
  }

  autoLogin(): any {
    const userData: {
      email: string,
      id: string,
      _token: string,
      _tokenExpirationDate: string
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return;
    }

    const loadedUser = new User(
      userData.email,
      userData.id,
      userData._token,
      new Date(userData._tokenExpirationDate)
    );

    if (loadedUser.token) {
      this.user.next(loadedUser);
      const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  private handleError(errorRes: HttpErrorResponse): any {
      let errorMessage = 'An unknown error occurred!';
      if (!errorRes.error || !errorRes.error.error) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.error.message) {
        case 'EMAIL_EXISTS':
          errorMessage = 'This email already exists!';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'This email does not exists!';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'This password is incorrect!';
          break;
      }
      return throwError(errorMessage);
   }
}


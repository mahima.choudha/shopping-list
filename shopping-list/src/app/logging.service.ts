import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class LoggingService {
  lastLog: string;

  printLog(message: string): any {
    console.log('New Log: ' + message);
    console.log('Last Log: ' + this.lastLog);
    this.lastLog = message;
  }
}

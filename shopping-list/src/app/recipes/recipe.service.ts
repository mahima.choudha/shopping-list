import {Recipe} from './recipe.model';
import {Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Subject} from 'rxjs';

@Injectable()
export class RecipeService {
  recipeChanged = new Subject<Recipe[]>();

  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Fondue',
  //     'Cheese & Bread',
  //     'https://food.fnr.sndimg.com/content/dam/images
  //     /food/fullset/2018/8/28/0/FNK_THREE-CHEESE-FONDUE-H_s4x3.jpg.rend.hgtvcom.826.620.suffix/1548181202472.jpeg',
  //     [
  //       new Ingredient('Cheese', 5),
  //       new Ingredient('Bread', 2)]),
  //   new Recipe(
  //     'Waffles',
  //     'Chocolate & Almond',
  //     'https://imagesvc.meredithcorp.io/v3/mm/image?
  //     url=https%3A%2F%2Fcdn-image.foodandwine.com%2
  //     Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2Frecipe1015-xl-loaded-potato-waffles.jpg%3Fitok%3DUur2szZg',
  //     [
  //       new Ingredient('Eggs', 6),
  //       new Ingredient('Milk', 1)])
  // ];
  private recipes: Recipe[] = [];

  constructor(private slService: ShoppingListService) {
  }
  getRecipes(): any {
    return this.recipes.slice();
  }

  getRecipe(index: number): any{
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]): any{
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe): any{
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe): any{
    this.recipes[index] = newRecipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number): any {
    this.recipes.splice(index);
    this.recipeChanged.next(this.recipes.slice());
  }

  setRecipes(recipes: Recipe[]): any {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice());
  }
}

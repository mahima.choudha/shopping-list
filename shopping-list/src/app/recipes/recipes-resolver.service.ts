import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Recipe} from './recipe.model';
import {DataStorageService} from '../shared/data-storage.service';
import {RecipeService} from './recipe.service';

@Injectable({providedIn: 'root'})
export class RecipesResolverService implements Resolve<Recipe>{

  constructor(private dsService: DataStorageService,
              private recipeService: RecipeService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const recipes = this.recipeService.getRecipes();
    if (recipes.length === 0) {
      // we don't need to subscribe here as resolve does that for us
      return this.dsService.fetchRecipes();
    } else {
      return recipes;
    }
  }

}

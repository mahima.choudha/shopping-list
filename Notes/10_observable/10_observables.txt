Observables -> 
1. An Observable doesn't have to complete.
2. Use it to handle asynchronous event.
3. Alternative of callbacks and Promises.
4. observables are constructs to which you subscribe to get informed about data. 
5 Observables are stream of data so when new data is emitted, our subscription will know about it.
6. In this.route.params.subscribe(), params is the observable.

Building new Observables ->
1. these are not baked in js or ts, it comes from package rxjs.
2. interval(1000 //milisec).subscribe(count => {
	console.log(count);		
});

// interval fires an event every 1000 miliseconds
// count gets the value that is emitted

3. To stop observables to keep emitting data, we need to unsubscribe these.
4. An unnecessary observable can slow down the application as it takes extra processing power.
5. Unsubscribing ->

private firstObsSubscription: Subscription;
	this.firstObsSubscription = interval(1000 //milisec).subscribe(count => {
	console.log(count);		
});

ngOnDestroy(): void{
	this.firstObsSubscription.unsubscribe();
	// it clears the subscription
}

6. We don't need to unsubscribe params as it is provided by angular and observables provided by angular are cleared an managed by angular.

------------------------------------------------------------------

Custom Observables ->
ngOnInit() {
	const customIntervalObservable = Observal.create(observer => {
let count =0;
	setInterval(handler: () =>{
observer.next(count); // next is used to emit new value
count++;
}, 1000);


this.firstcustomObservable = customIntervalObservable.subscribe(data => {
	console.log(data);
}
});
}

this.firstcustomObservable.unsubscribe(); //necessary with custom observable

observer -> the part that is interested in being informed about new data, error or observable being completed.

----------------------------------------------------------------------------

throw an error ->

observer.error(new Error('Count is greater than 3'));
- when observable throws an error, it doesn't need to be unsubscribed because after encountering an error, the observable dies.
- Handling Error ->>>>

this.firstcustomObservable = customIntervalObservable.subscribe(data => {
console.log(data);}, error => {
	console.log(error);
	alret(error.message);
});
});
}

-------------------------------------------------------------------

Completing observable ->>>
1. HttpRequest will complete
inside Observable.create ->
if(count == 2){
	observer.complete();
}

2. this.firstcustomObservable = customIntervalObservable.subscribe(data => {
console.log(data);}, error => {
	console.log(error);
	alret(error.message);
}, () => {
	console.log('complete');
});
});
}

2. unsubscription is not needed is observable is completed. but it is a good practice to unsubscribe it.

------------------------------------------------------------------





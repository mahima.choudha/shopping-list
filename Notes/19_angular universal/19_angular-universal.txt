1. Pre-renders pages before user visits the page.

2. When user visit the page, initial action doesn't happen in browser only subsequent actions will take place.

3. When user is on slower network, JS might take sometime to load and until the JS is downloaded, user will see nothing.

4. Add angular universal
 - indentifier in angualr.json file => projects: { "shopping-list" }
 - ng add @nguniversal/express-engine --clientProject shopping-list

5. code is first renderred on server so some APIs would fail. e.g. LocalStorage is a browser API which won't work on server.

6. To avoid this we need to put a check
- in app.component.ts, inject 
	@Inject(PLATFORM_ID) private platformId
	if (isPlatformBrowser(this.platformId)) {
		// call autologin
	}

** PLATFORM_ID and isPlatformBrowser are predefined in angular.

7. in angular.json, see the command in build:ssr, run this command to build angular universal for the app.
 
**  There is no property named build:ssr in angular 10

8. Run npm run build:ssr, this will build the app for angular universal.

** For this to work, server should be able to run nodejs

9. Run npm run serve:ssr

10. this will run on port 4000 on server.

11. this will show the console.log statements in browser console.


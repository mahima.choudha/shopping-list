1. Routing takes up some significant space in app module. to avoid this, create app-routing.module.ts in app folder.

2. app-routing.module.ts

** we are only using this module to out source our route to main module so import won't be enough

const appRoutes: Routes = [
	{path: '/', component: 'HomeComponent'}, ...
]
@NgModule({
	imports:[
		RouterModul.forRoot(appRoutes)
	],
	exports: [RouterModule] // allows to use routes to any module that imports this module
})
export class AppRoutingModule{}

3. Remove import of RouterModule from app.module and import it in to app-routing.module.ts

4. Finally, import AppRoutingModule inside app.module.ts imports: [] array.

-----------------------------------------------------------------

Very very IMP --> Lect. 146
1. Route Guards -> Protecting routes with canActive.
2. create auth-guard.service.ts in root

3. export class AuthGuard implements CanActivate {
	canActivate(route: ActivatedRouteSnapshot,
		    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
	this.authService.isAuthenticated().then(
	(authenticated: boolean) => {
		if (authenticated){
			return true;
		} else { this.router.navigate(['/']); }
	}
);
}
}
// angular should execute this code before a route is loaded
// canActivate can run asynchronously using Observable or Promise or Synchronously using boolean
// 

4. to use this route - app-routing.module.ts-
	{path: 'servers', canActivate: [AuthGuard], component ...}
5. servers are only accessible if the AuthGuard's canActivate method returns true which only happens if in authService isAuthenticated method returns true.

6. this will guard all the servers and it's children.

7. How to guard only child but not parent ----
- Implement CanActivateChild
- Override canActivateChild method.
- call this.canActivate from canActivateChild(..)
- {path: 'servers', canActivateChild: [AuthGuard], component ...}

-----------------------------------------------------------------

Very very IMP --> Lect. 147
Control if you are allowed to leave a route or not ------
1. A guard always have to be a service.
2. create service can-deactivate-guard.service.ts
3. can-deactivate-guard.service.ts -
export interface CanComponentDeactivate {
	canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
	canDeactivate(component: CanDComponentDeactivate,
	currentRoute: ActiveRouteSnapshot,
	currentState: RouteStateSnapshot,
	nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
	return component.canDeactivate();
}
}

4. {path: 'servers', component: ServersComponents, canDeactivate: [canDeactivateGuard], ...}

5. Add CanDeactivateGuard in provders in app module.
6. Need to implement CanDeactivateGuard in Servers component.
7. Override canDeactivate method and add logic.

